# Wiki Plugin


Wiki plugin for MedUX

## General

In a practice, Wikis help to organize the workflow, create SOPs, keep knowledge at one place. As MedUX aims to be a complete practice system, it is just straightforward that it should include a Wiki too.


## Install

You can install this plugin locally during development by invoking `pip install -e .` in this folder, assuming you have activated your main project's virtualenv.

## Models

Create your models as usual in `models.py`, they will be included. Don't forget to run `makemigrations` and `migrate` afterwords.


After each change regarding version number etc., run `python manage.py syncplugins`. This ensures your database is in sync with the plugins on disk.

## License

This plugin is licensed under the [GNU Affero General Public License v3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.txt)

