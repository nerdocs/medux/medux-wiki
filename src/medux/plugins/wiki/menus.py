from django.urls import reverse_lazy

from medux.common.api.interfaces import IMenuItem
from django.utils.translation import gettext_lazy as _


class Wiki(IMenuItem):
    menu = "views"
    title = _("Wiki")
    url = reverse_lazy("wiki:article-list")
    icon = "wikipedia"  # TODO: update Bootstrap/Tabler?
