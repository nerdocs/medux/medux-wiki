from django.urls import path
from . import views

# namespaced URLs
app_name = "wiki"
urlpatterns = [
    path("", views.ArticleList.as_view(), name="article-list"),
    path("article/add/", views.ArticleCreateView.as_view(), name="article-add"),
    # path(
    #     "article/by_author/<author>/",
    #     views.ArticleByAuthorView.as_view(),
    #     "article-by-author",
    # ),
    path(
        "article/<str:slug>/change/",
        views.ArticleUpdateView.as_view(),
        name="article-update",
    ),
    path("article/<str:slug>/", views.ArticleDetail.as_view(), name="article-detail"),
    path(
        "article/<str:slug>/publish/",
        views.ArticlePublishView.as_view(),
        name="article-publish",
    ),
    path(
        "article/<str:slug>/unpublish/",
        views.ArticleUnPublishView.as_view(),
        name="article-unpublish",
    ),
    path(
        "article/<str:slug>/history/",
        views.ArticleHistoryView.as_view(),
        name="article-history",
    ),
]


# global URLs
root_urlpatterns = [
    # path("api/foo", views.APIView.as_view(), name="api"),
]
