import reversion
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import CreateView, ListView, DetailView, UpdateView
from django.utils.translation import gettext_lazy as _
from reversion.models import Version
from reversion.views import RevisionMixin

from .models import Article
from .forms import ArticleForm


class ArticleCreateView(RevisionMixin, CreateView):
    template_name = "wiki/article_form.html"
    form_class = ArticleForm

    def form_valid(self, form):
        article = form.save(commit=False)
        article.author = self.request.user
        article.tenant = self.request.user.tenant
        article.save()
        msg = _("Article saved successfully.")
        messages.success(self.request, msg)
        return redirect(article)


class ArticleUpdateView(RevisionMixin, UpdateView):
    model = Article
    form_class = ArticleForm
    extra_context = {"editing": True}

    def form_valid(self, form):
        article = form.save(commit=False)
        article.save()
        reversion.set_user(self.request.user)
        reversion.set_comment(form.cleaned_data["comment"])
        msg = _("Article saved successfully")
        messages.success(self.request, msg)
        return redirect(article)


class ArticleHistoryView(RevisionMixin, ListView):
    template_name = "wiki/article_history.html"
    extra_context = {"history": True}

    def get_queryset(self):
        self.article = Article.objects.get(slug=self.kwargs.get("slug"))
        return Version.objects.get_for_object(self.article)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["article"] = get_object_or_404(Article, slug=self.kwargs.get("slug"))
        context["object_list"] = self.get_queryset()
        return context


class ArticleList(ListView):
    template_name = "wiki/article_list.html"

    def get_queryset(self):
        return Article.objects.all()


class ArticleDetail(DetailView):
    model = Article
    template_name = "wiki/article_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["last_version"] = Version.objects.get_for_object(self.object).first()
        return context


class ArticleByAuthorView(PermissionRequiredMixin, ListView):
    model = Article
    template_name = "wiki/article_list_by_author"
    permission_required = "wiki.view_article"

    def get_queryset(self):
        return self.model.objects.filter(
            tenant=self.request.user.tenant, author=self.request.user
        )

    def get_context_data(self, *, object_list=None, **kwargs):
        self.author = kwargs.pop("author")
        return super().get_context_data(object_list, **kwargs)


class ArticlePublishView(PermissionRequiredMixin, SuccessMessageMixin, DetailView):
    model = Article
    template_name = "wiki/_publish_button.html"
    permission_required = "wiki.change_article"
    success_message = _("Article published.")

    def post(self, request, *args, **kwargs):
        article = self.get_object()
        article.is_published = True
        article.save()
        return self.render_to_response({"article": article})


class ArticleUnPublishView(PermissionRequiredMixin, SuccessMessageMixin, DetailView):
    model = Article
    template_name = "wiki/_publish_button.html"
    permission_required = "wiki.change_article"
    success_message = _("Article unpublished.")

    def post(self, request, *args, **kwargs):
        article = self.get_object()
        article.is_published = False
        article.save()
        return self.render_to_response({"article": article})
