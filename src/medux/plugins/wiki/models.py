import reversion
from django.contrib.auth import get_user_model
from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from medux.common.models import Tenant, CreatedModifiedModel

User = get_user_model()


class PublishedArticlesManager(models.Manager):
    def get_query_set(self):
        return (
            super(PublishedArticlesManager, self)
            .get_query_set()
            .filter(is_published=True)
        )


@reversion.register()
class Article(CreatedModifiedModel):
    """Represents a wiki article"""

    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=50, unique=True)
    text = models.TextField(
        help_text="This text will be formatted using <a href='#' tabindex='-1'>MarkDown</a>."  # TODO: add link
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    is_published = models.BooleanField(default=False, verbose_name=_("Publish?"))

    objects = models.Manager()
    published = PublishedArticlesManager()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("wiki:article-detail", kwargs={"slug": self.slug})


class Edit(CreatedModifiedModel):
    """Stores an edit session"""

    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    editor = models.ForeignKey(User, on_delete=models.CASCADE)
    summary = models.CharField(max_length=100)

    class Meta:
        ordering = ["-modified"]

    def __str__(self):
        return f"{self.summary} - {self.editor} - {self.modified}"

    def get_absolute_url(self):
        return ("wiki:edit-detail", self.id)
