"""Wiki plugin for MedUX"""
# For plugin apps, the AppConfig is found automatically.
# default_app_config = (
#     "medux.plugins.wiki.apps.WikiConfig"
# )

__version__ = "0.0.1"
__license__ = "AGPLv3+"

INSTALLED_APPS = [
    "reversion",
]
