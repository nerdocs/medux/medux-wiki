from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit, Button, Div, HTML
from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Article, Edit


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        exclude = ["modified", "author", "slug", "tenant", "is_published"]

    comment = forms.CharField(
        max_length=255, help_text=_("A short summary of what you have changed.")
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        comment: forms.Field = self.fields["comment"]
        comment.required = True
        # comment.initial =
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", _("Save")))


class EditForm(forms.ModelForm):
    class Meta:
        model = Edit
        fields = ["summary"]
