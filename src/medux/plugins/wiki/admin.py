from django.contrib import admin
from reversion_compare.admin import VersionAdmin

# from reversion.admin import VersionAdmin

from .models import Article, Edit


@admin.register(Article)
class ArticleAdmin(VersionAdmin):
    pass


admin.site.register(Edit)
